package com.aeq.OE;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class WaranOEType3 {
	private WebDriver driver;
	private String url;
	private WebDriverWait wait;
	private WebElement element;
	private String jenis;
	private String noGiliran;
	private String status;
	private int num;
	private static ExtentReports reports1;
	private ExtentTest test;


	@BeforeClass
	private void setUp() {
		driver = new FirefoxDriver();
		url = "http://localhost:8080/budget/secured/create/";
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		reports1 = new ExtentReports("WaranOEType3_Results.html", true);
	}

	@Test(priority = 0)
	private void testTitle() {
		test = reports1.startTest("Test Started");
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=16&warrantType=3");
		test.log(LogStatus.INFO, "Application is opened in Browser.");
		String title = driver.getTitle();
		if (title.equalsIgnoreCase("Wujud Waran OE Tambahan")){
			Assert.assertTrue(title.contentEquals("Wujud Waran OE Tambahan"));
			test.log(LogStatus.PASS, "title displyed successfully");
		}
		else{
			Assert.fail("mismatches in title");
			test.log(LogStatus.ERROR, "title is not correct");
		}
	}

	@Test(priority = 1)
	public void initialFields() throws InterruptedException {
		element = driver.findElement(By.id("jenis"));
		element.sendKeys("type3"); // jenis value should be presented in document
		jenis = element.getAttribute("value");
		noGiliran = driver.findElement(By.id("noRujukan")).getAttribute("value");
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[6]/span[2]")).getText();
		Assert.assertEquals(status, "Draf");
		test.log(LogStatus.PASS, "At the beggining warant status is draf");
	}

	@Test(priority = 2)
	public void createTambah() throws InterruptedException {
		clickAnElementById("actionAdd");
		clickAnElementById("programId");
		selectProgramID("programId");
		int aktivitiSize = driver.findElements(By.xpath("//select[@id = 'activityselected']/option")).size();
		if (aktivitiSize == 1)
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		clickAnElementById("activityselected");
		selectFields("activityselected");
		clickAnElementById("typeOfActivitySelected");
		selectFields("typeOfActivitySelected");
		int dasarSize = driver.findElements(By.xpath("//select[@id = 'dasrvalue']/option")).size();
		if (dasarSize == 1)
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		clickAnElementById("dasrvalue");
		selectFields("dasrvalue");
		int objectAmSize = driver.findElements(By.xpath("//select[@id = 'objectAm']/option")).size();
		if (objectAmSize == 1)
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		clickAnElementById("objectAm");
		selectFields("objectAm");
		int sebagaiSize = driver.findElements(By.xpath("//select[@id = 'object2']/option")).size();
		if (sebagaiSize == 1)
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		clickAnElementById("object2");
		selectFields("object2");
		int negeriSize = driver.findElements(By.xpath("//select[@id = 'state']/option")).size();
		if (negeriSize == 1)
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		clickAnElementById("state");
		selectFields("state");
		driver.findElement(By.id("peruntukanDikehendaki")).sendKeys(getRandomNumberInRange(100, 150));
		driver.findElement(By.id("peruntukanDikehendaki")).sendKeys(Keys.ENTER);
		Assert.assertTrue(driver.findElement(By.id("jumlahBesarPeruntukan")).getText()
				.equals(driver.findElement(By.id("peruntukanDikehendaki")).getAttribute("value")));
		test.log(LogStatus.PASS, "Jumlah BesarPeruntukan value is equal to Peruntukan Dikehendaki value");
		driver.findElement(By.id("saveOnly")).click();
		if (driver.getPageSource().contains("Sila Pilih Program")
				|| driver.getPageSource().contains("Sila Pilih Aktiviti")
				|| driver.getPageSource().contains("Sila Pilih Dasar")
				|| driver.getPageSource().contains("Sila Pilih objectAm")
				|| driver.getPageSource().contains("Sila Pilih Objek Sebagai")
				|| driver.getPageSource().contains("Sila Pilih Negeri")) {
			Assert.fail("please fill required tambha fields");
			test.log(LogStatus.ERROR, "The  required tambah field has not filled");
		} else {
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equalsIgnoreCase("Berjaya disimpan"));
			clickAnElementByXpath("//div[contains(@class, 'ui-dialog-buttonset')]/button/span");
			test.log(LogStatus.PASS, "The tambah fields added succesfully");
			Thread.sleep(500);
		}
	}

	@Test(priority = 3)
	public void otherFields() throws InterruptedException {
		int namaPelulusSize = driver.findElements(By.xpath("//select[@id ='approverId']/option")).size();
		if (namaPelulusSize != 0) {
			Select namaPelulus = new Select(driver.findElement(By.id("approverId")));
			num = new Random().nextInt(namaPelulus.getOptions().size());
			if (num == 0) {
				num += 1;
				namaPelulus.selectByIndex(num);
			} else
				namaPelulus.selectByIndex(num);
		}
		Thread.sleep(500);
		driver.findElement(By.id("skMembers_0")).sendKeys("Test1");
		uploadFile();
		driver.findElement(By.id("comments")).sendKeys("File uploaded");
		Thread.sleep(500);
		// Hantar
		driver.findElement(By.id("actionTypeSubmit")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().equalsIgnoreCase("Fail berjaya dimuat naik Berjaya?."));
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().contains("Berjaya Permohonan berjaya dihantar."));
		test.log(LogStatus.PASS, "File has been submited successfully");
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
	}

	@Test(priority = 4)
	public void search() throws InterruptedException {
		driver.findElement(By.id("actionType")).click();
		driver.findElement(By.xpath("//table[@id = 'budgetInfotable']/tbody/tr/td/a")).click();
		Thread.sleep(1000);
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("rujukanNo=" + noGiliran)) {
			Assert.assertTrue(currentUrl.contains(noGiliran), "RujukanNumber appended with url");
			test.log(LogStatus.PASS, "File searching completed");
		} else {
			Assert.fail("Rujukan number not append with url");
			test.log(LogStatus.ERROR, "Rujukan number is not showing in url");
		}
	}

	@Test(priority = 5)
	public void successReview() throws InterruptedException {
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[6]/span[2]")).getText();
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=17&warrantType=3&rujukanNo=" + noGiliran);
		test.log(LogStatus.INFO, "The url redirected to review page");
		Assert.assertTrue(jenis.contains(driver.findElement(By.id("jenis")).getAttribute("value")));
		test.log(LogStatus.INFO, "The given jenis value is shown in review page");
		if (status.equalsIgnoreCase("Untuk Disemak"))
			test.log(LogStatus.INFO, "status is Untuk Disemak");
		Thread.sleep(1000);
		edit();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@id ='addDiv_0']/a/img")).click();
		driver.findElement(By.id("skMembers")).clear();
		driver.findElement(By.id("skMembers")).sendKeys("Test2");
		driver.findElement(By.id("comments")).sendKeys("review succes");
		driver.findElement(By.id("actionType")).click();
		Assert.assertEquals(driver
				.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(),
				"Fail berjaya dimuat naik Simpan?.");
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		test.log(LogStatus.PASS, "Review successfully completed");
		if (driver.getPageSource().contains("No. Rujukan (Waran)") && driver.getPageSource().contains("Luar Jangka Bil")
				&& driver.getPageSource().contains("Tarikh Lulus")) {
			status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]")).getText();
			Assert.assertTrue(status.equalsIgnoreCase("Telah Disemak"));
			test.log(LogStatus.INFO, "After successful review the status is Telah Disemak");
			Thread.sleep(1000);
		} else {
			Assert.fail("3 main fields are not present");
			test.log(LogStatus.FAIL, "No. Rujukan (Waran),Waran Khas Bil,Tarikh Lulus fields are missing");
		}
	}

	@Test(priority = 6)
	public void afterReview() throws InterruptedException {
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=16&warrantType=3&rujukanNo=" + noGiliran + "#0");
		Thread.sleep(1000);
		test.log(LogStatus.INFO,
				"Browser redirected to create warant page for fillups the 3 fields(No. Rujukan ,Waran Khas Bil,Tarikh Lulus)");
		clickAnElementById("permohanan");
		driver.findElement(By.id("permohanan")).sendKeys(getRandomNumberInRange(1, 10));
		driver.findElement(By.id("waranKhasBil")).sendKeys(getRandomNumberInRange(1, 20));
		driver.findElement(By.id("approveDate")).click();
		WebElement datePicker = driver.findElement(By.id("ui-datepicker-div"));
		List<WebElement> noOfColumns = datePicker.findElements(By.tagName("td"));
		for (WebElement cell : noOfColumns) {
			// Select the date from date picker when condition match
			if (cell.getText().equals(currentDate())) {
				cell.findElement(By.linkText(currentDate())).click();
				break;
			}
		}
		driver.findElement(By.id("comments")).sendKeys("rujukanno, waran bill and date fields completed");
		driver.findElement(By.id("actionType")).click();
		if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText()
				.contains("Sila masukkan No.Rujukan!")) {
			Assert.fail("Rujukan number field can't be empty");
			test.log(LogStatus.ERROR, "Rujukan number field is empty");
		} else if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().equalsIgnoreCase("Sila masukkan ulasan")) {
			Assert.fail("Comment(Komen) field can't be empty");
			test.log(LogStatus.ERROR, "Komen field is empty");
		} else if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().contentEquals("Sila masukkan No.Waran Khas Bil!")) {
			Assert.fail("Waran Khas bill field can't be empty");
			test.log(LogStatus.ERROR, "Waran Khas bill field is empty");
		} else {
			Assert.assertEquals(driver
					.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(),
					"Fail berjaya dimuat naik Simpan?.");
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "No. Rujukan (Waran),Waran Khas Bil,Tarikh Lulus fields are filled successfully");
			Thread.sleep(1000);
		}
	}

	@Test(priority = 7)
	public void approval() throws InterruptedException {
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=17&warrantType=3&rujukanNo=" + noGiliran);
		Thread.sleep(1000);
		test.log(LogStatus.INFO, "Browser redirected to approval page");
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]")).getText();
		if (status.equalsIgnoreCase("Waran Telah Dimuat Naik")) {
			test.log(LogStatus.INFO, "Status is Waran Telah Dimuat Naik");
			driver.findElement(By.id("comments")).sendKeys("Final approval success");
			driver.findElement(By.id("actionType")).click();
			Thread.sleep(1000);
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equals("Fail berjaya dimuat naik Disahkan?."));
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equals("Disahkan Permohonan berjaya dihantar."));
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "Its been approved");
			Thread.sleep(1000);
		}
	}

	// Upload a file
	private void uploadFile() throws InterruptedException {
		driver.findElement(By.id("UploadFile")).sendKeys("/home/aequalis/Downloads/Net.pdf");
		Thread.sleep(500);
		clickAnElementById("muatNaik");
		if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText()
				.equals("Fail yang dimuat naik tidak boleh melebihi 5MB")) {
			Assert.fail("Uploaded files should not exceed 5MB");
			test.log(LogStatus.ERROR, "Attached file size is exceed the limit");
		} else {
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equalsIgnoreCase("Fail berjaya dimuat naik"));
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "File uploaded success");
			Thread.sleep(1000);
		}
	}

	private void edit() throws InterruptedException {
		if (driver.findElement(By.id("budgetInfotable_info")).getText()
				.equalsIgnoreCase("Showing 1 to 1 of 1 entries")) {
			clickAnElementByXpath("//table[@id='budgetInfotable']/tbody/tr/td[11]/a");
			clickAnElementById("programId");
			selectProgramID("programId");
			int aktivitiSize = driver.findElements(By.xpath("//select[@id = 'activityselected']/option")).size();
			if (aktivitiSize == 1)
				driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			clickAnElementById("activityselected");
			selectFields("activityselected");
			clickAnElementById("typeOfActivitySelected");
			selectFields("typeOfActivitySelected");
			int dasarSize = driver.findElements(By.xpath("//select[@id = 'dasrvalue']/option")).size();
			if (dasarSize == 1)
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			clickAnElementById("dasrvalue");
			selectFields("dasrvalue");
			int objectAmSize = driver.findElements(By.xpath("//select[@id = 'objectAm']/option")).size();
			if (objectAmSize == 1)
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			clickAnElementById("objectAm");
			selectFields("objectAm");
			int sebagaiSize = driver.findElements(By.xpath("//select[@id = 'object2']/option")).size();
			if (sebagaiSize == 1)
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			clickAnElementById("object2");
			selectFields("object2");
			int negeriSize = driver.findElements(By.xpath("//select[@id = 'state']/option")).size();
			if (negeriSize == 1)
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			clickAnElementById("state");
			selectFields("state");
			driver.findElement(By.id("peruntukanDikehendaki")).clear();
			driver.findElement(By.id("peruntukanDikehendaki")).sendKeys(getRandomNumberInRange(100, 150));
			driver.findElement(By.id("saveOnly")).click();
			if (driver.getPageSource().contains("Sila Pilih Program")
					|| driver.getPageSource().contains("Sila Pilih Aktiviti")
					|| driver.getPageSource().contains("Sila Pilih Dasar")
					|| driver.getPageSource().contains("Sila Pilih objectAm")
					|| driver.getPageSource().contains("Sila Pilih Objek Sebagai")
					|| driver.getPageSource().contains("Sila Pilih Negeri")) {
				Assert.fail("please fill required tambha fields");
				test.log(LogStatus.ERROR, "please fill required tambha fields");
			} else {
				Assert.assertTrue(
						driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
								.getText().equalsIgnoreCase("Berjaya disimpan"));
				clickAnElementByXpath("//div[contains(@class, 'ui-dialog-buttonset')]/button/span");
				test.log(LogStatus.PASS, "successfully edit and saved tambha fields");
				Thread.sleep(500);
			}
		}
	}
	

	private void clickAnElementByXpath(String xpath) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		driver.findElement(By.xpath(xpath)).click();
	}

	private void clickAnElementById(String id) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
		driver.findElement(By.id(id)).click();
	}

	private String currentDate() {
		String PATTERN = "dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String currentDate = dateFormat.format(Calendar.getInstance().getTime());
		int date = Integer.parseInt(currentDate);
		return Integer.toString(date);
	}

	private static String getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		int random = r.ints(min, (max + 1)).limit(1).findFirst().getAsInt();
		return Integer.toString(random);
	}

	private void selectProgramID(String id) {
		Select select = new Select(driver.findElement(By.id(id)));
		num = new Random().nextInt(select.getOptions().size());
		if (num == 0) {
			num = num + 1;
			select.selectByIndex(num);
		} else if (num == 9 || num == 10 || num == 11) {
			num = Integer.parseInt(getRandomNumberInRange(1, 8));
			select.selectByIndex(num);
		} else
			select.selectByIndex(num);
	}

	private void selectFields(String id) {
		Select select = new Select(driver.findElement(By.id(id)));
		num = new Random().nextInt(select.getOptions().size());
		if (num == 0) {
			num = num + 1;
			select.selectByIndex(num);
		} else
			select.selectByIndex(num);
	}
	
	@AfterClass
	public void quit(){
		reports1.endTest(test);
		// writing everything to document
		reports1.flush();
		driver.quit();
	}

}
