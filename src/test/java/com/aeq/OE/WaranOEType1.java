package com.aeq.OE;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class WaranOEType1 {
	private WebDriver driver;
	private String url;
	private WebElement element;
	private Select select;
	private String noGiliran;
	private String status;
	private String jenisValue;
	private WebDriverWait wait;
	private static ExtentReports reports1;
	private ExtentTest test;

	@BeforeClass
	private void setUp() {
		driver = new FirefoxDriver();
		url = "http://localhost:8080/budget/secured/create/";
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		reports1 = new ExtentReports("WaranOEType1_Results.html", true);
	}

	@Test(priority = 0)
	private void testTitle() {
		test = reports1.startTest("Test Started");
		driver.get(url
				+ "warrant?ministryId=225&userId=5001955&roleId=16&kodVot=T11&namaMaksud=Waran%20Tambahan%20Peruntukan%20Mengurus&warrantType=1");
		test.log(LogStatus.INFO, "Application is opened in Browser.");
		String title = driver.getTitle();
		
		if (title.equalsIgnoreCase("Wujud Waran OE Tambahan")){
			Assert.assertTrue(true);
			test.log(LogStatus.PASS, "title displayed");
		}
		else{
			Assert.fail("mismatches in title");
			test.log(LogStatus.ERROR, "title is not correct");
		}
	}

	@Test(priority = 1)
	public void startUpFields() {
		driver.get(url
				+ "warrant?ministryId=225&userId=5001955&roleId=16&kodVot=T11&namaMaksud=Waran%20Tambahan%20Peruntukan%20Mengurus&warrantType=1");
		WebElement jenis = driver.findElement(By.id("jenis"));
		jenis.sendKeys("create"); // jenis value should be presented in document
		jenisValue = jenis.getAttribute("value");
		noGiliran = driver.findElement(By.id("noRujukan")).getAttribute("value");
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[6]/span[2]")).getText();
		Assert.assertEquals(status, "Draf");
		test.log(LogStatus.PASS, "Warant status is draf");
	}

	// create method using Simpan
	@Test(priority = 2)
	private void createTambah() throws InterruptedException {
		driver.findElement(By.id("actionAdd")).click();
		driver.findElement(By.id("programId")).click();
		Select program = new Select(driver.findElement(By.id("programId")));
		program.selectByValue("29");
		Thread.sleep(500);
		driver.findElement(By.id("activityselected")).click();
		Select aktiviti = new Select(driver.findElement(By.id("activityselected")));
		aktiviti.selectByValue("803");
		Thread.sleep(1000);
		driver.findElement(By.id("typeOfActivitySelected")).click();
		Select typeOfActivity = new Select(driver.findElement(By.id("typeOfActivitySelected")));
		typeOfActivity.selectByValue("1");
		Thread.sleep(1000);
		driver.findElement(By.id("dasrvalue")).click();
		Select dasar = new Select(driver.findElement(By.id("dasrvalue")));
		dasar.selectByValue("1");
		Thread.sleep(1000);
		driver.findElement(By.id("objectAm")).click();
		Select objekAM = new Select(driver.findElement(By.id("objectAm")));
		objekAM.selectByValue("1");
		Thread.sleep(1000);
		driver.findElement(By.id("object2")).click();
		Select objekSebagai = new Select(driver.findElement(By.id("object2")));
		objekSebagai.selectByValue("37");
		Thread.sleep(1000);
		driver.findElement(By.id("state")).click();
		Select negeri = new Select(driver.findElement(By.id("state")));
		negeri.selectByValue("1");
		Thread.sleep(1000);
		WebElement peruntukanDikehendakiSekarang = driver.findElement(By.id("peruntukanDikehendaki"));
		peruntukanDikehendakiSekarang.sendKeys(getRandomNumberInRange(1000, 1500));
		peruntukanDikehendakiSekarang.sendKeys(Keys.ENTER);
		if (driver.getPageSource().contains("Only Numeric"))
			Assert.fail("Numeric values only accepted");
		String newPeruntukan = addComma(peruntukanDikehendakiSekarang.getAttribute("value"));
		String jumlah = driver.findElement(By.id("jumlahBesarPeruntukan")).getText();
		Assert.assertEquals(jumlah, newPeruntukan);
		driver.findElement(By.id("saveOnly")).click();
		if (driver.getPageSource().contains("Sila Pilih Program")
				|| driver.getPageSource().contains("Sila Pilih Aktiviti")
				|| driver.getPageSource().contains("Sila Pilih Dasar")
				|| driver.getPageSource().contains("Sila Pilih objectAm")
				|| driver.getPageSource().contains("Sila Pilih Objek Sebagai")
				|| driver.getPageSource().contains("Sila Pilih Negeri")){
			Assert.fail("please fill required tambha fields");
			test.log(LogStatus.ERROR, "The  required tambah field has not filled");
		}
		else {
			Assert.assertEquals("Berjaya disimpan", driver
					.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText());
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button/span")).click();
			test.log(LogStatus.PASS, "The tambah fields added succesfully");
			Thread.sleep(500);
			//Assert.assertEquals("Showing 1 to 1 of 1 entries", driver.findElement(By.id("budgetInfotable_info")).getText());
		}
	}

	// fill other than required fields
	@Test(priority = 3)
	public void otherFields() throws InterruptedException {
		select = new Select(driver.findElement(By.id("approverId")));
		select.selectByValue("AB.RAHMAN BIN MAT-Timbalan Pengarah Bahagian Belanjawan");
		driver.findElement(By.id("skMembers_0")).sendKeys("Test1");
		uploadFile();
		driver.findElement(By.id("comments")).sendKeys("File uploaded");
		Thread.sleep(500);
		// Hantar
		driver.findElement(By.id("actionTypeSubmit")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().equalsIgnoreCase("Fail berjaya dimuat naik Berjaya?."));
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().contains("Berjaya Permohonan berjaya dihantar."));
		test.log(LogStatus.PASS, "File has been submited successfully");
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
	}

	// Search and select created list
	@Test(priority = 4)
	public void search() throws InterruptedException {
		driver.findElement(By.id("actionType")).click();
		driver.findElement(By.xpath("//table[@id = 'budgetInfotable']/tbody/tr/td/a")).click();
		Thread.sleep(1000);
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("rujukanNo=" + noGiliran)) {
			Assert.assertTrue(currentUrl.contains(noGiliran), "RujukanNumber appended with url");
			test.log(LogStatus.PASS, "File searching completed");
		} else {
			Assert.fail("Rujukan number not append with url");
			test.log(LogStatus.ERROR, "Rujukan number is not showing in url");
		}
	}

	@Test(priority = 5)
	public void successReview() throws InterruptedException {
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[6]/span[2]")).getText();
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=17&warrantType=1&rujukanNo=" + noGiliran);
		test.log(LogStatus.INFO, "The url redirected to review page");
		Assert.assertTrue(jenisValue.contains(driver.findElement(By.id("jenis")).getAttribute("value")));
		test.log(LogStatus.INFO, "The given jenis value is shown in review page");
		if (status.equalsIgnoreCase("Untuk Disemak"))
			test.log(LogStatus.INFO, "status is Untuk Disemak");
		Thread.sleep(1000);
		editTambah();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@id ='addDiv_0']/a/img")).click();
		driver.findElement(By.id("skMembers")).clear();
		driver.findElement(By.id("skMembers")).sendKeys("Test2");
		driver.findElement(By.id("comments")).sendKeys("review succes");
		driver.findElement(By.id("actionType")).click();
		Assert.assertEquals(driver
				.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(),
				"Fail berjaya dimuat naik Simpan?.");
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		test.log(LogStatus.PASS, "Review successfully completed");
		if (driver.getPageSource().contains("No. Rujukan (Waran)") && driver.getPageSource().contains("Waran Khas Bil")
				&& driver.getPageSource().contains("Tarikh Lulus")) {
			status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]")).getText();
			Assert.assertTrue(status.equalsIgnoreCase("Telah Disemak"));
			test.log(LogStatus.INFO, "status changed to Telah Disemak");
			Thread.sleep(1000);
		} else {
			Assert.fail("3 main fields are not present");
			test.log(LogStatus.FAIL, "No. Rujukan (Waran),Waran Khas Bil,Tarikh Lulus fields are missing");
		}
	}

	@Test(priority = 6)
	public void afterReview() throws InterruptedException {
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=16&warrantType=1&rujukanNo=" + noGiliran + "#0");
		Thread.sleep(1000);
		test.log(LogStatus.INFO,
				"Browser redirected to create warant page for fillups the 3 fields(No. Rujukan ,Waran Khas Bil,Tarikh Lulus)");
		clickAnElementById("permohanan");
		Thread.sleep(1000);
		driver.findElement(By.id("permohanan")).sendKeys(getRandomNumberInRange(1, 10));
		driver.findElement(By.id("waranKhasBil")).sendKeys(getRandomNumberInRange(10, 15));
		driver.findElement(By.id("approveDate")).click();
		WebElement datePicker = driver.findElement(By.id("ui-datepicker-div"));
		List<WebElement> noOfColumns = datePicker.findElements(By.tagName("td"));
		for (WebElement cell : noOfColumns) {
			// Select the date from date picker when condition match
			if (cell.getText().equals(currentDate())) {
				cell.findElement(By.linkText(currentDate())).click();
				break;
			}
		}
		// driver.findElement(By.id("approveDate")).sendKeys(Keys.ENTER);
		driver.findElement(By.id("comments")).sendKeys("rujukanno, waran bill and date fields completed");
		driver.findElement(By.id("actionType")).click();
		if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText()
				.contains("Sila masukkan No.Rujukan!")) {
			Assert.fail("Rujukan number field can't be empty");
			test.log(LogStatus.ERROR, "Rujukan number field is empty");
		} else if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().equalsIgnoreCase("Sila masukkan ulasan")) {
			Assert.fail("Comment(Komen) field can't be empty");
			test.log(LogStatus.ERROR, "Komen field is empty");
		} else if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().contentEquals("Sila masukkan No.Waran Khas Bil!")) {
			Assert.fail("Waran Khas bill field can't be empty");
			test.log(LogStatus.ERROR, "Waran Khas bill field is empty");
		} else {
			Assert.assertEquals(driver
					.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(),
					"Fail berjaya dimuat naik Simpan?.");
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "No. Rujukan (Waran),Waran Khas Bil,Tarikh Lulus fields are filled successfully");
			Thread.sleep(1000);
		}
	}

	@Test(priority = 7)
	public void approval() throws InterruptedException {
		driver.get(url + "warrant?ministryId=225&userId=5001955&roleId=17&warrantType=1&rujukanNo=" + noGiliran);
		// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		Thread.sleep(1000);
		test.log(LogStatus.INFO, "Browser redirected to approval page");
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]")).getText();
		if (status.equalsIgnoreCase("Waran Telah Dimuat Naik")) {
			test.log(LogStatus.INFO, "Status is Waran Telah Dimuat Naik");
			driver.findElement(By.id("comments")).sendKeys("Final approval success");
			driver.findElement(By.id("actionType")).click();
			Thread.sleep(1000);
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equals("Fail berjaya dimuat naik Disahkan?."));
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equals("Disahkan Permohonan berjaya dihantar."));
			test.log(LogStatus.PASS, "Its been approved");
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			Thread.sleep(1000);
		}
	}

	// Upload a file
	private void uploadFile() throws InterruptedException {
		driver.findElement(By.id("UploadFile")).sendKeys("/home/aequalis/Downloads/Net.pdf");
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[contains(@class, 'wrnt-section')]/div[3]/input")).click();
		if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText()
				.equals("Fail yang dimuat naik tidak boleh melebihi 5MB")) {
			Assert.fail("Uploaded files should not exceed 5MB");
			test.log(LogStatus.ERROR, "Attached file size is exceed the limit");
		} else {
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equalsIgnoreCase("Fail berjaya dimuat naik"));
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "File uploaded success");
			Thread.sleep(500);
		}
	}

	private void editTambah() throws InterruptedException {
		if (driver.findElement(By.id("budgetInfotable_info")).getText()
				.equalsIgnoreCase("Showing 1 to 1 of 1 entries")) {
			Assert.assertTrue(true);
			clickAnElementByXpath("//table[@id='budgetInfotable']/tbody/tr/td[11]/a");
			// driver.findElement(By.xpath("//table[@id='budgetInfotable']/tbody/tr/td[11]/a")).click();
			select = new Select(driver.findElement(By.id("programId")));
			element = select.getFirstSelectedOption();
			if (element.getText().equalsIgnoreCase("-----Sila Pilih------"))
				Assert.fail("value not saved");
			else
				select.selectByValue("31");
			select = new Select(driver.findElement(By.id("activityselected")));
			element = select.getFirstSelectedOption();
			Thread.sleep(500);
			if (element.getText().equalsIgnoreCase("-----Sila Pilih------"))
				select.selectByValue("814");
			else
				Assert.fail("old record is not yet changed ");
			select = new Select(driver.findElement(By.id("objectAm")));
			element = select.getFirstSelectedOption();
			if (element.getText().equalsIgnoreCase("10000 -- EMOLUMEN"))
				select.selectByValue("3");
			else
				Assert.fail("no record found to be edit");
			select = new Select(driver.findElement(By.id("object2")));
			element = select.getFirstSelectedOption();
			Thread.sleep(500);
			if (element.getText().equalsIgnoreCase("-----Sila Pilih------"))
				select.selectByValue("17");
			else
				Assert.fail("old record is not yet changed ");
			element = driver.findElement(By.id("peruntukanDikehendaki"));
			element.clear();
			element.sendKeys(getRandomNumberInRange(50, 100));
			element.sendKeys(Keys.ENTER);
			String newPeruntukanValue = addComma(element.getAttribute("value"));
			String jumlahValue = driver.findElement(By.id("jumlahBesarPeruntukan")).getText();
			if (jumlahValue.equals(newPeruntukanValue)){
				Assert.assertEquals(jumlahValue, newPeruntukanValue);
				test.log(LogStatus.PASS, "JumlahValue and NewPeruntukanValue is equal");
				driver.findElement(By.id("saveOnly")).click();
				Thread.sleep(1000);
				Assert.assertEquals("Berjaya disimpan", driver
						.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText());
				driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button/span")).click();
				test.log(LogStatus.PASS, "successfully edit and saved tambha fields");
			}
			else{
				Assert.fail("Jumlah value not equal to Peruntukan");
				test.log(LogStatus.ERROR, "JumlahValue and NewPeruntukanValue is not equal");
			}
		} else if (driver.findElement(By.id("budgetInfotable_info")).getText()
				.equalsIgnoreCase("Showing 0 to 0 of 0 entries")) {
			Assert.fail("No record found to be edit");
			test.log(LogStatus.INFO, "No records found to be edit");
		}
	}

	private String addComma(String value) {
		int number = Integer.parseInt(value);
		DecimalFormat formatt = new DecimalFormat("#,###");
		return formatt.format(number).toString();
	}

	private String currentDate() {
		String PATTERN = "dd";
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String currentDate = dateFormat.format(Calendar.getInstance().getTime());
		int date = Integer.parseInt(currentDate);
		return Integer.toString(date);
	}
	
	private static String getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		int random = r.ints(min, (max + 1)).limit(1).findFirst().getAsInt();
		return Integer.toString(random);
	}

	private void clickAnElementById(String id) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
		driver.findElement(By.id(id)).click();
	}

	private void clickAnElementByXpath(String xpath) {
		wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		driver.findElement(By.xpath(xpath)).click();
	}

	/*// Delete
	private void delete() {
		if (driver.findElement(By.id("budgetInfotable_info")).getText()
				.equalsIgnoreCase("Showing 1 to 1 of 1 entries")) {
			driver.findElement(By.xpath("//table[@id='budgetInfotable']/tbody/tr/td[11]/a[2]")).click();
			Assert.assertEquals(driver
					.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(),
					"Anda pasti untuk menghapuskan maklumat tersebut?");
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		} else if (driver.findElement(By.id("budgetInfotable_info")).getText()
				.equalsIgnoreCase("Showing 0 to 0 of 0 entries")) {
			Assert.fail("No record found to edit");
		}
	}*/

	// Quit browser
	@AfterClass
	public void quit() {
		reports1.endTest(test);
		// writing everything to document
		reports1.flush();
		driver.quit();
	}

}
