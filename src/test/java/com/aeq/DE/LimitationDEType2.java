package com.aeq.DE;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LimitationDEType2 {

	private WebDriver driver;
	private String url;
	private WebDriverWait wait;
	private WebElement element;
	private String jenis;
	private String noGiliran;
	private String status;
	private Select select;
	private static ExtentReports reports;
	private ExtentTest test;
	@BeforeClass
	private void setUp() {
		driver = new FirefoxDriver();
		url = "http://localhost:8080/budget/secured/create/";
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		reports = new ExtentReports("LimitationDEType2_Results.html", true);
	}
	
	@Test(priority = 0)
	private void testTitle() {
		test = reports.startTest("Verify Home page");
		driver.get(url+"limitationDE?ministryId=225&userId=5001955&roleId=16&warrantType=2");
		String title = driver.getTitle();
		System.out.println(title);
		if (title.equalsIgnoreCase("Wujud Waran DE Sekatan")){
			Assert.assertTrue(title.contentEquals("Wujud Waran DE Sekatan"));
			test.log(LogStatus.PASS, "Home page title is displayed.");
		}
		else{
			Assert.fail("mismatches in title");
			test.log(LogStatus.ERROR, "Mismatches in title");
		}
			
	}
	
	@Test(priority = 1)
	public void initialFields() throws InterruptedException{
		element = driver.findElement(By.id("jenis"));
		element.sendKeys("limit"); // jenis value should be presented in document
		jenis=element.getAttribute("value");
		noGiliran = driver.findElement(By.id("noRujukan")).getAttribute("value");
		status = driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[6]/span[2]"))
				.getText();
		test.log(LogStatus.PASS, "Status is equal to draf");
	}
	
	@Test(priority = 2)
	private void tambah() throws InterruptedException{
		clickAnElementById("actionAdd");
		clickAnElementById("projectId");
		Select projectId=new Select(driver.findElement(By.id("projectId")));
		projectId.selectByIndex(new Random().nextInt(projectId.getOptions().size()));
		Thread.sleep(500);
		clickAnElementById("caraPembiyaanselected");
		Select cara=new Select(driver.findElement(By.id("caraPembiyaanselected")));
		cara.selectByIndex(new Random().nextInt(cara.getOptions().size()));
		Thread.sleep(500);
		//Assert.assertTrue(driver.findElement(By.id("peruntukanDiluluskan")).getText().equals("0"));
		driver.findElement(By.id("earlierApplicationAmtId")).sendKeys(getRandomNumberInRange(500, 600));
		driver.findElement(By.id("earlierApplicationAmtId")).sendKeys(Keys.ENTER);
		driver.findElement(By.id("firstApplicationAmtId")).sendKeys(getRandomNumberInRange(100, 150));
		driver.findElement(By.id("firstApplicationAmtId")).sendKeys(Keys.ENTER);
		int jumlah=Integer.parseInt(driver.findElement(By.id("firstApplicationAmtId")).getAttribute("value"))+Integer.parseInt(driver.findElement(By.id("firstApplicationAmtId")).getAttribute("value"));
		if(jumlah==Integer.parseInt(driver.findElement(By.id("bakiPermohonan")).getText())){
			Assert.assertEquals(driver.findElement(By.id("bakiPermohonan")).getText(), jumlah);
			test.log(LogStatus.PASS, "jumlah value is equal to sum of Permohonan Terdahulu and Pertama");
		}
		driver.findElement(By.id("saveOnly")).click();
		if (driver.getPageSource().contains("Sila Pilih the Projek ")
				|| driver.getPageSource().contains("Please enter the Permohonan Terdahulu (RM)")
				|| driver.getPageSource().contains("Please enter the Permohonan Pertama")) {
			Assert.fail("please fill required tambha fields");
			test.log(LogStatus.ERROR, "Required tambha fields are missing");
		}
		else{
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().equalsIgnoreCase("Berjaya disimpan"));
		clickAnElementByXpath("//div[contains(@class, 'ui-dialog-buttonset')]/button/span");
		test.log(LogStatus.PASS, "Tambah fields added successfully");
		Thread.sleep(500);
		}
	}
	
	@Test(priority = 3)
	public void otherFields() throws InterruptedException {
		Select namaPelulus = new Select(driver.findElement(By.id("approverId")));
		namaPelulus.selectByIndex(new Random().nextInt(namaPelulus.getOptions().size()));
		driver.findElement(By.id("skMembers_0")).sendKeys("Test1");
		uploadFile();
		driver.findElement(By.id("comments")).sendKeys("File uploaded");
		Thread.sleep(500);
		// Hantar
		driver.findElement(By.id("actionTypeSubmit")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().equalsIgnoreCase("Fail berjaya dimuat naik Berjaya?."));
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
				.getText().contains("Berjaya Permohonan berjaya dihantar."));
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		test.log(LogStatus.PASS, "file submition succes");
	}
	
	@Test(priority = 4)
	public void search() throws InterruptedException{
		driver.findElement(By.id("actionType")).click();
		driver.findElement(By.xpath("//table[@id = 'budgetInfotable']/tbody/tr/td/a")).click();
		test.log(LogStatus.INFO, "The url redirected to review page");
		Thread.sleep(1000);
		String currentUrl=driver.getCurrentUrl();
		if(currentUrl.contains("rujukanNo="+noGiliran)){
			Assert.assertTrue(currentUrl.contains(noGiliran), "RujukanNumber appended with url");
			test.log(LogStatus.PASS, "Rujukan number added in url");
		}
		else {
			Assert.fail("Rujukan number not append with url");
			test.log(LogStatus.FAIL, "Rujukan number is not showing in url");
		}
	}
	
	@Test(priority = 5)
	public void successReview() throws InterruptedException{
		status= driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[6]/span[2]"))
				.getText();
		//System.out.println(status);
		driver.get(url+"limitationDE?ministryId=225&userId=5001955&roleId=17&warrantType=2&rujukanNo="+noGiliran);
		test.log(LogStatus.INFO, "The url redirected to review page");
		Assert.assertTrue(jenis.contains(driver.findElement(By.id("jenis")).getAttribute("value")));
		test.log(LogStatus.INFO, "The given jenis value is shown in review page");
		Thread.sleep(1000);
		edit();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@id ='addDiv_0']/a/img")).click();
		driver.findElement(By.id("skMembers")).clear();
		driver.findElement(By.id("skMembers")).sendKeys("Test2");
		driver.findElement(By.id("comments")).sendKeys("review succes");
		driver.findElement(By.id("actionType")).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(), "Fail berjaya dimuat naik Simpan?.");
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		if(driver.getPageSource().contains("No. Rujukan (Waran)") && driver.getPageSource().contains("Waran Khas Bil") && driver.getPageSource().contains("Tarikh Lulus")){
			status=driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]"))
					.getText();
			Assert.assertTrue(status.equalsIgnoreCase("Telah Disemak"));
			test.log(LogStatus.INFO, "status changed to Telah Disemak");
			Thread.sleep(1000);
		}
		else{
			Assert.fail("3 main fields are not present");
			test.log(LogStatus.FAIL, "No. Rujukan (Waran),Waran Khas Bil,Tarikh Lulus fields are missing");
		}
	}
	
	@Test(priority = 6)
	public void afterReview() throws InterruptedException{
		driver.get(url+"limitationDE?ministryId=225&userId=5001955&roleId=16&warrantType=2&rujukanNo="+noGiliran+"#0");
		test.log(LogStatus.INFO,
				"Browser redirected to limitation page for fillups the 3 fields(No. Rujukan ,Waran Khas Bil,Tarikh Lulus)");
		Thread.sleep(1000);
		clickAnElementById("permohanan");
		//Thread.sleep(1000);
		driver.findElement(By.id("permohanan")).sendKeys(getRandomNumberInRange(1, 10));
		driver.findElement(By.id("waranKhasBil")).sendKeys(getRandomNumberInRange(1, 20));
		driver.findElement(By.id("approveDate")).click();
		WebElement datePicker = driver.findElement(By.id("ui-datepicker-div"));
		List<WebElement> noOfColumns = datePicker.findElements(By.tagName("td"));  
		for (WebElement cell: noOfColumns){
			   if (cell.getText().equals(currentDate())){
			    cell.findElement(By.linkText(currentDate())).click();
			    break; 
	        }
	    }
		driver.findElement(By.id("comments")).sendKeys("rujukanno, waran bill and date fields completed");
		driver.findElement(By.id("actionType")).click();
		if(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().contains("Sila masukkan No.Rujukan!")){
			Assert.fail("Rujukan number field can't be empty");
			test.log(LogStatus.ERROR, "Rujukan number field is empty");
		}
		else if(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().equalsIgnoreCase("Sila masukkan ulasan")){
			Assert.fail("Comment(Komen) field can't be empty");
			test.log(LogStatus.ERROR, "Komen field is empty");
		}
		else if(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().contentEquals("Sila masukkan No.Waran Khas Bil!")){
			Assert.fail("Waran Khas bill field can't be empty");
			test.log(LogStatus.ERROR, "Waran Khas bill field is empty");
		}
		else{
			Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText(),"Fail berjaya dimuat naik Simpan?.");
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "No. Rujukan (Waran),Waran Khas Bil,Tarikh Lulus fields are filled successfully");
			Thread.sleep(1000);
			}
		}
	
	@Test(priority = 7)
	public void approval() throws InterruptedException{
	 driver.get(url+"limitationDE?ministryId=225&userId=5001955&roleId=17&warrantType=2&rujukanNo="+noGiliran);
	// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	 test.log(LogStatus.INFO, "Browser redirected to approval page");
	 Thread.sleep(1000);
	 status=driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]"))
				.getText();
	 if(status.equalsIgnoreCase("Waran Telah Dimuat Naik")){
		 test.log(LogStatus.INFO, "Status is Waran Telah Dimuat Naik");
		 driver.findElement(By.id("comments")).sendKeys("Final approval success");
		 driver.findElement(By.id("actionType")).click();
		 Thread.sleep(1000);
		 Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().equals("Fail berjaya dimuat naik Disahkan?."));
		 driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		 Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().equals("Disahkan Permohonan berjaya dihantar."));
		 driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
		 Thread.sleep(1000);
		 status=driver.findElement(By.xpath("//div[contains(@class, 'section-body')]/div[10]/span[2]")).getText();
			if(status.equals("Diluluskan")){
				Assert.assertEquals(status, "Diluluskan");
				test.log(LogStatus.INFO, "final satus is Diluluskan");
				test.log(LogStatus.PASS, "Its been approved");
			}else{
				test.log(LogStatus.INFO, "status is not equal to Diluluskan");
				
			}
		}
	}
	
	private void clickAnElementByXpath(String xpath) {
		wait = new WebDriverWait(driver, 20);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	    driver.findElement(By.xpath(xpath)).click();
	}
	
	private void clickAnElementById(String id) {
		wait = new WebDriverWait(driver, 20);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
	    driver.findElement(By.id(id)).click();
	}
	
	private static String getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		int random= r.ints(min, (max + 1)).limit(1).findFirst().getAsInt();
		return Integer.toString(random);
	}
	
	private String currentDate(){
		String PATTERN="dd";
		SimpleDateFormat dateFormat=new SimpleDateFormat();
		dateFormat.applyPattern(PATTERN);
		String currentDate=dateFormat.format(Calendar.getInstance().getTime());
		int date =Integer.parseInt(currentDate);
		return Integer.toString(date);
	}
	
	private void edit() throws InterruptedException {
		if (driver.findElement(By.id("budgetInfotable_info")).getText()
				.equalsIgnoreCase("Showing 1 to 1 of 1 entries")) {
			clickAnElementByXpath("//table[@id='budgetInfotable']/tbody/tr/td[9]/a");
			select = new Select(driver.findElement(By.id("projectId")));
			element = select.getFirstSelectedOption();
			if (element.getText().equalsIgnoreCase("-----Sila Pilih------"))
				Assert.fail("value not saved");
			else
				select.selectByIndex(new Random().nextInt(select.getOptions().size()));
			select = new Select(driver.findElement(By.id("caraPembiyaanselected")));
			// Assert.assertEquals(driver.findElement(By.id("peruntukanDikehendaki")).getAttribute("value"),
			// driver.findElement(By.id("jumlahBesarPeruntukan")).getText());
			driver.findElement(By.id("earlierApplicationAmtId")).clear();
			driver.findElement(By.id("earlierApplicationAmtId")).sendKeys(getRandomNumberInRange(500, 600));
			driver.findElement(By.id("firstApplicationAmtId")).clear();
			driver.findElement(By.id("firstApplicationAmtId")).sendKeys(getRandomNumberInRange(100, 150));
			driver.findElement(By.id("saveOnly")).click();
			if (driver.getPageSource().contains("Sila Pilih the Projek ")
					|| driver.getPageSource().contains("Please enter the Permohonan Terdahulu (RM)")
					|| driver.getPageSource().contains("Please enter the Permohonan Pertama")) {
				Assert.fail("please fill required tambha fields");
				test.log(LogStatus.ERROR, "Required tambha fields are missing");
			}
			else{
			Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText().equalsIgnoreCase("Berjaya disimpan"));
			clickAnElementByXpath("//div[contains(@class, 'ui-dialog-buttonset')]/button/span");
			test.log(LogStatus.PASS, "Tambah fields edited successfully");
			Thread.sleep(500);
			}
		}
	}
	// Upload a file
	private void uploadFile() throws InterruptedException {
		driver.findElement(By.id("UploadFile")).sendKeys("/home/aequalis/Downloads/Net.pdf");
		Thread.sleep(500);
		clickAnElementByXpath(".//*[@id='createWarrant']/div[1]/div[10]/div[3]/input");
		if (driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]")).getText()
				.equals("Fail yang dimuat naik tidak boleh melebihi 5MB")) {
			Assert.fail("Uploaded files should not exceed 5MB");
			test.log(LogStatus.ERROR, "File size  exceeded the limit");
		} else {
			Assert.assertTrue(
					driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-content ui-widget-content')]"))
							.getText().equalsIgnoreCase("Fail berjaya dimuat naik"));
			driver.findElement(By.xpath("//div[contains(@class, 'ui-dialog-buttonset')]/button")).click();
			test.log(LogStatus.PASS, "File uploaded successfully");
			Thread.sleep(1000);
		}
	}
	
	@AfterClass
	public void quit(){
		reports.endTest(test);
		// writing everything to document
		reports.flush();
		driver.quit();
	}

}

